# React + Keycloak demo app

1. Run keycloak (`docker compose up`)
2. create `demo` realm
3. create `demo-web` client:
* access type: public
* valid redirect uri: `http://localhost:3000/*`
* Web Origins: `http://localhost:3000`
* `keycloak.json` already contains configuration for the web app
4. run react app:

    npm start

5. open http://localhost:3000