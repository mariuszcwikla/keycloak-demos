import logo from './logo.svg';
import './App.css';

//split string into array of strings each of 40 characters long
function split80(str) {
  var result = [];
  var temp = "";
  for (var i = 0; i < str.length; i++) {
    if (temp.length < 40) {
      temp += str[i];
    } else {
      result.push(temp);
      temp = "";
    }
  }
  result.push(temp);
  return result;
}

function App(props) {
  let parts = props.keycloak.token.split(".");
  let header =  atob(parts[0]);
  let payload = atob(parts[1]);
  let tokdiv = split80(props.keycloak.token)
    .map(str => <div>{str}</div>);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
          <button onClick={() => props.keycloak.login({
            acr: {
              value: "gold",
              transactionId: "1234"
            }
          })}>
            acr gold + transactionId
          </button>
        </div>
        <div>
          <div>JWT Header</div>
          <div>{header}</div>
        </div>
        <div>
          <div>JWT payload</div>
          <div>{payload}</div>
        </div>
        <div>
          <div>RAW JWT token</div>
          <div>{tokdiv}</div>
        </div>
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
