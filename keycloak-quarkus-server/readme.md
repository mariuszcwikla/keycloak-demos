This is "intermediary" maven module which is responsible for creating my own distribution of Keycloak with certain 
Quarkus extensions added.

It creates a zip file. Then the zip file is unpacked in keycloak-container module.

Why is it done 2-step? Because I want to copy kafka+quarkus to `/opt/keycloak/lib` and copy SPIs to `/opt/keycloak/providers`.

Having this in one maven module will be very difficult to separate it. With the zip file approach it's easy to manage this separation.

Alternatively instead of Maven Assembly plugin we could just create a "base" image that adds Quarkus extensions.