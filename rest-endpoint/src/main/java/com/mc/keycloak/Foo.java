package com.mc.keycloak;

import javax.enterprise.context.ApplicationScoped;

import io.quarkus.arc.Unremovable;

@ApplicationScoped
@Unremovable
public class Foo {

	public Foo() {
		System.out.println("foo");
	}

	public String bar() {
		return "baz";
	}
}
