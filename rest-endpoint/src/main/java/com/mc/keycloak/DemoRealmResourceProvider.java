package com.mc.keycloak;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.keycloak.common.util.Time;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.protocol.oidc.OIDCAdvancedConfigWrapper;
import org.keycloak.protocol.oidc.OIDCLoginProtocol;
import org.keycloak.representations.LogoutToken;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.util.JsonSerialization;
import org.keycloak.util.TokenUtil;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class DemoRealmResourceProvider implements RealmResourceProvider {

	private final KeycloakSession session;

	@Override
	public void close() {
	}

	@Override
	public Object getResource() {
		return this;
	}

	// curl -v http://localhost:8080/realms/master/mc/demo
	@GET
	@Path("demo")
	@Produces(MediaType.APPLICATION_JSON)
	public DemoResponse getDemoResponse() {
		return new DemoResponse("hello");
	}
	
	@GET
	@Path("mytoken/{login}")
	@Produces(MediaType.APPLICATION_JSON)
	public MyTokenResponse getMyToken(final @PathParam("login") String login/**UUID myId*/) {
		session.getContext().getRealm();
        MyToken token = new MyToken();
        token.type("MyTokenType");
        token.subject(login);
        token.id(KeycloakModelUtils.generateId());
        token.issuedNow();
        token.issuedFor("demo-client");
        token.exp((long) (Time.currentTime() + 60 * 5));		//valid for 5 minutes
        
        String encoded = session.tokens().encode(token);
        System.out.println("Generated token: " + encoded);

        MyTokenResponse response = new MyTokenResponse();
        response.setMyToken(encoded);
        return response;
	}
}
