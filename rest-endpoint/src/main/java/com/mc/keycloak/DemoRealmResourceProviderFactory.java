package com.mc.keycloak;

import javax.enterprise.inject.spi.CDI;

import org.eclipse.microprofile.config.ConfigProvider;
import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

import io.smallrye.config.ConfigValue;
import io.smallrye.config.SmallRyeConfig;

public class DemoRealmResourceProviderFactory implements RealmResourceProviderFactory {

	@Override
	public RealmResourceProvider create(KeycloakSession session) {
		SmallRyeConfig config = ConfigProvider.getConfig().unwrap(SmallRyeConfig.class);
		ConfigValue cv = config.getConfigValue("foo.property1");

		FooConfig fooConfig = config.getConfigMapping(FooConfig.class);
		System.out.println(String.format("Config: %s %d", fooConfig.property1(), fooConfig.property2()));
		
		DemoRealmResourceProvider res = new DemoRealmResourceProvider(session);
		Foo foo = CDI.current().select(Foo.class).get();
		System.out.println(foo.bar());
		return res;
	}

	@Override
	public void init(Config.Scope config) {
	}

	@Override
	public void postInit(KeycloakSessionFactory factory) {
	}

	@Override
	public void close() {
	}

	@Override
	public String getId() {
		return "mc";
	}

}
