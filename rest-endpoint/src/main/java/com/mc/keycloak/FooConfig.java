package com.mc.keycloak;

import io.quarkus.arc.Unremovable;
import io.smallrye.config.ConfigMapping;

@ConfigMapping(prefix = "foo")
@Unremovable
public interface FooConfig {

	String property1();
	
	int property2();
}
