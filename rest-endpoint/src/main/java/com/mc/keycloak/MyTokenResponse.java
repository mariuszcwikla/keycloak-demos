package com.mc.keycloak;

import lombok.Data;

@Data
public class MyTokenResponse {

	private String myToken;

}
