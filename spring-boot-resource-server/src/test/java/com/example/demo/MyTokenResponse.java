package com.example.demo;

import lombok.Data;

@Data
public class MyTokenResponse {

	private String myToken;

}
