package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


import static io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

class IntegrationTest {

	@Test
	void test401() {
		given()
			.baseUri("http://localhost:8081")
		.when()
			.get("foo")
		.then()
			.statusCode(401);
	}

	
	@Test
	public void test200() {
		//First create a user "alice" in keycloak (manually)
		String jwt = given()
			.baseUri("http://localhost:8080/realms/master/mc")
		.when()
			.get("/mytoken/alice")
		.then()
			.statusCode(200)
			.and()
			.extract()
			.as(MyTokenResponse.class)
			.getMyToken();

		given()
			.baseUri("http://localhost:8081")
			.auth().oauth2(jwt)
		.when()
			.get("foo")
		.then()
			.statusCode(200);
	}
}
