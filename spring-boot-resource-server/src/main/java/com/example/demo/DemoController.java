package com.example.demo;

import javax.servlet.http.HttpServletRequest;

import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

	@Autowired
	private HttpServletRequest httpServletRequest;
	@GetMapping("/foo")
	public String foo() {
		Object obj = httpServletRequest.getAttribute(KeycloakSecurityContext.class.getName());
		return "foo";
	}
	@GetMapping("/bar")
	public String bar() {
		Object obj = httpServletRequest.getAttribute(KeycloakSecurityContext.class.getName());
		return "foo";
	}
}
