package com.mc;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.microsoft.sqlserver.jdbc.StringUtils;

import io.quarkus.runtime.Quarkus;

public class KeycloakRunner {

	public static void main(String[] args) {
		/**
		 * This runner does not run kafka to avoid spamming in longs.
		 * To run kafka example, use KeycloakRunnerKafka
		 */
		Path p = Paths.get(".", "kc-home");
		if (!p.toFile().exists()) {
			p.toFile().mkdir();
		}
		String cwd = p.toAbsolutePath().normalize().toString();
		System.setProperty("kc.home.dir", cwd);
		//see QuarkusKeycloakApplication (kc 19), KeycloakMain (kc 20)
		
		// See also System.setProperty("keycloak.import", filesToImport.toString());
		
		String filesToImport = Paths.get(".").toAbsolutePath().normalize().toString() + "/src/main/resources/demo-realm-export.json";
		System.setProperty("keycloak.import", filesToImport.toString());
		Quarkus.run(
				"--verbose",
				"start-dev",
				"--log-level=info,"
						+ "liquibase:info,"
						+ "com.mc:debug",
				"--import-realm"
				);
	}

}
