package com.mc;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.TransactionPhase;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.KeycloakTransactionManager;
import org.keycloak.quarkus.runtime.integration.QuarkusKeycloakSessionFactory;
import org.keycloak.services.ServicesLogger;
import org.keycloak.services.managers.ApplianceBootstrap;
import org.keycloak.services.resources.KeycloakApplication;

import io.quarkus.arc.Priority;
import io.quarkus.runtime.StartupEvent;
import io.smallrye.config.Priorities;
import lombok.extern.jbosslog.JBossLog;

@ApplicationScoped
@JBossLog
public class CreateAdminUser {

	static final int PRIORITY = Priorities.APPLICATION + 100;

	void onStart(@Observes(during = TransactionPhase.AFTER_SUCCESS)
				 @Priority(value = PRIORITY)
				StartupEvent ev) {
		//Children, don't do it on production.
		//But seriously. Usually this bean gets called after Keycloak is initialized
		//But sometimes it's called before which results in NPE.
		//The workaround is to do this thread + loop.
		new Thread( () -> {
			while(KeycloakApplication.getSessionFactory() == null) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
					return;
				}
			}
			createAdminUser(KeycloakApplication.getSessionFactory());
		}).start();
	}
	
//    @PostConstruct
//    void init() {
//    	QuarkusKeycloakSessionFactory sessionFactory = QuarkusKeycloakSessionFactory.getInstance();
//    	createAdminUser(sessionFactory);
//    }
    
	
	void createAdminUser(KeycloakSessionFactory sessionFactory ) {
    	//This is rough copy of org.keycloak.quarkus.runtime.KeycloakMain.createAdminUser() private method. 
    	//createAdminUser relies on on setting up env variables which is not difficult
    	//but I want to have fully-automated way to create admin user for development purposes.
    	//sessionFactory = QuarkusKeycloakSessionFactory.getInstance();
		
		//Fuck.. does not work nicely. Often I get NPE here
		//Because of some ordering of the @Observes
		//Sometimes works, sometimes does not.
		//Maybe this will work https://stackoverflow.com/a/74612325
        String adminUserName = "admin";
        String adminPassword = "admin";

//        KeycloakSessionFactory sessionFactory = KeycloakApplication.getSessionFactory();
        KeycloakSession session = sessionFactory.create();
        KeycloakTransactionManager transaction = session.getTransactionManager();

        try {
            transaction.begin();

            new ApplianceBootstrap(session).createMasterRealmUser(adminUserName, adminPassword);
            ServicesLogger.LOGGER.addUserSuccess(adminUserName, Config.getAdminRealm());

            transaction.commit();
        } catch (IllegalStateException e) {
            session.getTransactionManager().rollback();
            ServicesLogger.LOGGER.addUserFailedUserExists(adminUserName, Config.getAdminRealm());
        } catch (Throwable t) {
            session.getTransactionManager().rollback();
            ServicesLogger.LOGGER.addUserFailed(t, adminUserName, Config.getAdminRealm());
        } finally {
            session.close();
        }
    }
}
