package com.mc;

import com.microsoft.sqlserver.jdbc.StringUtils;

import io.quarkus.runtime.Quarkus;

public class KeycloakRunnerCockroach {

	public static void main(String[] args) {
		//see QuarkusKeycloakApplication (kc 19), KeycloakMain (kc 20)
		if (StringUtils.isEmpty(System.getenv("KEYCLOAK_ADMIN"))
			|| StringUtils.isEmpty(System.getenv("KEYCLOAK_ADMIN_PASSWORD")))
			throw new RuntimeException("Please configure KEYCLOAK_ADMIN and KEYCLOAK_ADMIN_PASSWORD");

		System.setProperty("quarkus.profile", "croach");

		Quarkus.run(
				"--verbose",
				"start-dev",
				"--storage=jpa",
				"--features=map-storage",
				"--db=postgres",
				"--db-url-host=localhost",
				"--db-url-port=26250",
				"--db-username=keycloak",
				"--db-password=keycloak",
				"--log-level=info,liquibase:debug"
				);

	}

}
