package com.mc.keycloak;

import static org.junit.Assert.assertTrue;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;

import javax.net.ServerSocketFactory;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;

import dasniko.testcontainers.keycloak.KeycloakContainer;

class KeycloakTests {
	
	private static Keycloak client;

	@BeforeAll
	static void setUp() {
		client = Fixture.getInstance().getClient();
	}

	@Test
	void smokeTest() {
		Integer count = client.realm("master")
			.users()
			.count();
		assertTrue(count > 0);
	}

}
