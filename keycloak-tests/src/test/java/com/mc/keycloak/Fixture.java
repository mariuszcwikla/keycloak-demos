package com.mc.keycloak;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.Collections;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import dasniko.testcontainers.keycloak.KeycloakContainer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Fixture {

	private KeycloakContainer keycloak;
	private Keycloak client;
	private int port;
	
	private static Fixture instance;
	private static String hostname;

	private Fixture(KeycloakContainer keycloak, Keycloak client, int port) {
		this.keycloak = keycloak;
		this.client = client;
		this.port = port;
	}
	
	public static boolean isPortAvailable(int port) {
		ServerSocket ss = null;
	    DatagramSocket ds = null;
	    try {
	        ss = new ServerSocket(port);
	        ss.setReuseAddress(true);
	        ds = new DatagramSocket(port);
	        ds.setReuseAddress(true);
	        return true;
	    } catch (IOException e) {
	    } finally {
	        if (ds != null) {
	            ds.close();
	        }

	        if (ss != null) {
	            try {
	                ss.close();
	            } catch (IOException e) {
	                /* should not be thrown */
	            }
	        }
	    }

	    return false;
	}
	
	public static synchronized Fixture getInstance() {
		if (instance == null) {
			//run "docker compose up" - then test will run much faster
			/*
			 * port 8081 is used by docker-compose
			 * If port 
			 */
			KeycloakContainer keycloak = null;
			Keycloak client;
			int port;
			//jenkins runs on port 8080 ;)...
			if (isRunningOnJenkins() || isPortAvailable(8080)) {
				keycloak = new KeycloakContainer();
				keycloak.start();
				client = keycloak.getKeycloakAdminClient();
				port = keycloak.getMappedPort(8080);
				hostname = keycloak.getHost();		//hostname on jenkins is "docker", not "localhost"
				log.debug("Keycloak started at {}:{}", hostname, port);
			} else {
				client = Keycloak.getInstance("http://localhost:8080", "master", "admin", "admin", "admin-cli");
				port = 8080;
				hostname = "localhost";
			}

			instance = new Fixture(keycloak, client, port);
		}
		return instance;
	}

	private static boolean isRunningOnJenkins() {
		return !StringUtils.isBlank(System.getenv("BUILD_NUMBER"));
	}

	public Keycloak getClient() {
		return client;
	}

	public int getPort() {
		return port;
	}
	
	public String getHost() {
		return hostname;
	}

	public void deleteUser(String username) {
		client.realm("master").users().search(username)
			.forEach( u -> client.realm("master").users().delete(u.getId()));
	}

	public void createUser(String username, String password) {
		UserRepresentation rep = new UserRepresentation();
		rep.setUsername(username);
		rep.setEnabled(true);
		CredentialRepresentation cred = new CredentialRepresentation();
		cred.setType(CredentialRepresentation.PASSWORD);
		cred.setValue(password);
		rep.setCredentials(Collections.singletonList(cred));
		Response response = client.realm("master").users().create(rep);
		assertEquals(201, response.getStatus());
	}

}
