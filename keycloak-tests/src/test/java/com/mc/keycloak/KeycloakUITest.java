package com.mc.keycloak;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.time.Duration;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
class KeycloakUITest {

	private WebDriver driver;

	@BeforeAll
	static void setupClass() {
		WebDriverManager.chromedriver().setup();
	}
	@BeforeEach
	public void setUp() {
		ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("window-size=1920,1080");
		driver = new ChromeDriver(options);
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(500));

		Fixture fixture = Fixture.getInstance();
		fixture.deleteUser("foo");
		fixture.createUser("foo", "foo");
	}

	@AfterEach
	public void tearDown(TestInfo ti) throws IOException {
		if (driver != null) {
			makeScreenshot(ti.getTestMethod().map( t -> t.getName()) +  ".png");
			driver.quit();
		}
	}

	@Test
	void testLoginViaSigninButton() throws InterruptedException, IOException {
		int port = Fixture.getInstance().getPort();
		String host = Fixture.getInstance().getHost();
		String accountConsoleUrl = "http://" + host + ":" + port + "/realms/master/account";
		log.debug("account console url: {}", accountConsoleUrl);
		driver.navigate().to(accountConsoleUrl);
		

		WebElement signinButton = new WebDriverWait(driver, Duration.ofSeconds(30))
		        .until(ExpectedConditions.elementToBeClickable(By.id("landingSignInButton")));
		makeScreenshot("1.png");
		signinButton.click();

		assertEquals("Sign in to Keycloak", driver.getTitle());
		
		WebElement username = driver.findElement(By.id("username"));
		WebElement password = driver.findElement(By.id("password"));
		WebElement loginButton = driver.findElement(By.id("kc-login"));
		
		username.sendKeys("foo");
		password.sendKeys("foo");

		makeScreenshot("2.png");
		loginButton.click();

		makeScreenshot("3.png");
		assertEquals("Keycloak Account Management", driver.getTitle());

	}
	
	@Test
	void testLoginPagePattern() throws InterruptedException, IOException {
		int port = Fixture.getInstance().getPort();
		String host = Fixture.getInstance().getHost();
		String accountConsoleUrl = "http://" + host + ":" + port + "/realms/master/account";
		log.debug("account console url: {}", accountConsoleUrl);
		driver.navigate().to(accountConsoleUrl);
		
		
		WebElement signinButton = new WebDriverWait(driver, Duration.ofSeconds(30))
		        .until(ExpectedConditions.elementToBeClickable(By.id("landingSignInButton")));
		signinButton.click();

		assertEquals("Sign in to Keycloak", driver.getTitle());
		
		LoginPage login = new LoginPage(driver);
		login.login("foo", "foo");

		assertEquals("Keycloak Account Management", driver.getTitle());

	}
	private void makeScreenshot(String fileName) throws IOException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		File targetFile = new File("./" + fileName);
        FileUtils.copyFile(scrFile, targetFile);
        log.info("file 1: {}", scrFile.toString());
        log.info("file 2: {}", targetFile);
		
	}
	@Test
	public void testGoogle() {
		driver.navigate().to("http://google.com");
		assertEquals("Google", driver.getTitle());
	}

	@Test
	@Disabled("Not working yet")
	void testLogin() throws MalformedURLException {
		String baseUrl = "http://localhost:8080/realms/master/protocol/openid-connect/auth";
		URI uri = UriBuilder.fromUri(baseUrl)
			.queryParam("client_id", "account-console")
			.queryParam("redirect_uri", "http://localhost:8080/realms/master/account/")
			.queryParam("state", "1234")
			.queryParam("scope", "openid")
			.queryParam("response_type", "code")
			.queryParam("code_challenge", "1234")
			.queryParam("code_challenge_method", "S256")
			.build();
			;
		//http://localhost:8080/realms/master/protocol/openid-connect/auth?client_id=account-console&
//			redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Frealms%2Fmaster%2Faccount%2F%3Ferror%3Dinvalid_request%26error_description%3DMissing%2Bparameter%253A%2Bresponse_type%26state%3D1234%23%2F
//					&state=9d70e99d-eb43-4cec-9ed0-cfb0670791d5
//					&response_mode=fragment
//					&response_type=code
//					&scope=openid
//					&nonce=8b3d3e58-ccd4-4848-bc01-71ae2f35123e
//					&code_challenge=ZIV-WIi0grI-sCP2A2R64hStBGu0cQCGBV6wroqHk2c
//					&code_challenge_method=S256
		//"?client_id=account-console&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Frealms%2Fmaster%2Faccount%2F%23%2F&state=1f3ccb83-1db5-4610-b852-01e802543eee&response_mode=fragment&response_type=code&scope=openid&nonce=61817317-6547-4c1a-8836-5165f9ebbeea&code_challenge=DuxDvLWJXZ0Ra302MYy6NNl09fQ2Wmu4mnj9HyyXmZc&code_challenge_method=S256
		log.debug("{}", uri);
		driver.navigate().to(uri.toURL());
	}

}
