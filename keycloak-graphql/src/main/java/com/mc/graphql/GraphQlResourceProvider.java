package com.mc.graphql;

import java.io.StringReader;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonReaderFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.services.resource.RealmResourceProvider;

import io.smallrye.graphql.execution.ExecutionResponse;
import io.smallrye.graphql.execution.ExecutionResponseWriter;
import io.smallrye.graphql.execution.ExecutionService;
import io.smallrye.graphql.execution.JsonObjectResponseWriter;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class GraphQlResourceProvider implements RealmResourceProvider {

	private final KeycloakSession session;

	private final ExecutionService executionService;

	protected static final JsonReaderFactory jsonReaderFactory = Json.createReaderFactory(null);

	@Override
	public void close() {
	}

	@Override
	public Object getResource() {
		return this;
	}

    protected JsonObject inputToJsonObject(String input) {
        try (JsonReader jsonReader = jsonReaderFactory.createReader(new StringReader(input))) {
            return jsonReader.readObject();
        }
    }

    /*
     * curl --location --request POST 'localhost:8080/realms/master/graphql' \
			--header 'Content-Type: application/json' \
			--data-raw '{"query":"query{\r\n    allFilms\r\n}","variables":{}}'
		
		GraphQL is also available at localhost:8080/graphql but this one has some drawbacks (access to KeycloakSession is limited - no info about the realm)
		
		Schema is deployed by quarkus extension under http://localhost:8080/graphql/schema.graphql
		If we want to expose it under http://localhost:8080/realms/master/graphql/schema.graphql then have a look at
		io.quarkus.smallrye.graphql.deployment.SmallRyeGraphQLProcessor.buildSchemaEndpoint
     */
    
    static class MyExecutionResponseWriter implements ExecutionResponseWriter{

		String response;

		@Override
		public void write(ExecutionResponse er) {
			this.response = er.getExecutionResultAsString();
		}
    	
    }
    
	@POST
	@Path("/")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getDemoResponse(String body) {
        
		MyExecutionResponseWriter writer = new MyExecutionResponseWriter();
		executionService.execute(inputToJsonObject(body), writer, false);
		return Response.ok(writer.response).build();
	}

}
