package com.mc.graphql;

import org.keycloak.Config;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.services.resource.RealmResourceProvider;
import org.keycloak.services.resource.RealmResourceProviderFactory;

import io.quarkus.arc.Arc;
import io.smallrye.graphql.execution.ExecutionService;

public class GraphQlRealmResourceProviderFactory implements RealmResourceProviderFactory {

	private ExecutionService executionService;

	@Override
	public RealmResourceProvider create(KeycloakSession session) {
		GraphQlResourceProvider res = new GraphQlResourceProvider(session, executionService);
		return res;
	}

	@Override
	public void init(Config.Scope config) {
	}

	@Override
	public void postInit(KeycloakSessionFactory factory) {
		executionService = Arc.container().instance(ExecutionService.class).get();
	}

	@Override
	public void close() {
	}

	@Override
	public String getId() {
		return "graphql";
	}

}
