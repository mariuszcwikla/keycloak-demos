package com.mc.graphql;

import org.keycloak.common.util.Resteasy;
import org.keycloak.models.KeycloakSession;

public class KeycloakSessionHolder {
	public static KeycloakSession getSession() {
		return Resteasy.getContextData(KeycloakSession.class);
	}
}
