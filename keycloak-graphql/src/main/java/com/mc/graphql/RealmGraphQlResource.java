package com.mc.graphql;

import javax.ws.rs.core.Context;

import org.eclipse.microprofile.graphql.GraphQLApi;
import org.eclipse.microprofile.graphql.Query;
import org.keycloak.common.util.Resteasy;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;

@GraphQLApi 
public class RealmGraphQlResource {

    @Query("realm")  
    public String getRealmName() {
    	/**
    	 * Few notes:
    	 * - we can't use KeycloakModelUtils.runJobInTransaction
    	 * because this will create new KeycloakSession object without information
    	 * about current realm.
    	 * - instead ThreadLocal is utilized to pass KeycloakSession object around
    	 * 
    	 * Secondly: graphql endpoint is actually deployed twice
    	 * 1. on /graphql - this comes from graphql extension itself
    	 * 2. on /realms/<realm-name>/graphql - this endpoint comes from the GraphqlResourceProvider
    	 * 
    	 * TODO Is there easy way to disable #1? We don't need this endpoint. Besides endpoint #1 will result in NPE
    	 * because the realm is not set. Realm is sent on KeycloakContext by keycloak internals, 
    	 * see org.keycloak.services.resources.RealmsResource.init(String)  
    	 * Potential solution would be to disable "build steps" from SmallRyeGraphQLProcessor 
    	 * See also https://github.com/quarkusio/quarkus/issues/3233
    	 */
    	
    	KeycloakSession session = KeycloakSessionHolder.getSession();
		RealmModel realm = session.getContext().getRealm();
    	return realm.getName();
    }
}