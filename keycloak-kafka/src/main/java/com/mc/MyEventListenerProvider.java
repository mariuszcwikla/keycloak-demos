package com.mc;

import java.util.UUID;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.Uuid;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

import io.smallrye.reactive.messaging.kafka.KafkaClientService;
import io.smallrye.reactive.messaging.kafka.KafkaProducer;

public class MyEventListenerProvider implements EventListenerProvider {

	private KeycloakSession session;
	private KafkaClientService kafkaService;
	private KafkaProducer<String, LoginEvent> producer;

	public MyEventListenerProvider(KeycloakSession session, KafkaClientService kafkaService) {
		this.session = session;
		this.kafkaService = kafkaService;
		this.producer = kafkaService.getProducer("login-events");
	}

	@Override
	public void close() {

	}

	/*
	 * How to test it:
	 * 
	 * 1. Go to keycloak -> realm -> Events and configure `mc-eventlistener`
	 * 2. create user
	 * 3. log in as a user, e.g. http://localhost:8080/realms/master/account/
	 * 4. check kafka
	 * 
	 * 		 kcat -t login-events -f '%k: %s\n'
	 */
	@Override
	public void onEvent(Event event) {
		if (true) {
			//skipping it. Stuck at login if kafka is not running.
			return;
		}
		switch(event.getType()) {
			case LOGIN:
			{
				UUID userId = UUID.fromString(event.getUserId());
				RealmModel realm = session.realms().getRealm(event.getRealmId());
				UserModel user = session.users().getUserById(realm, event.getUserId());
				producer.send(
						new ProducerRecord<String, LoginEvent>(
								"login-events",
								event.getUserId(), 
								LoginEvent.builder()
										.userId(userId)
										.login(user.getUsername())
										.email(user.getEmail()
									).build()))
				.await()
				.indefinitely();
			}
			default:
				
		}
	}

	@Override
	public void onEvent(AdminEvent event, boolean includeRepresentation) {
		// TODO Auto-generated method stub

	}

}
