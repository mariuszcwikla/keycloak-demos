package com.mc;

import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.transaction.Transactional;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.quarkus.runtime.integration.QuarkusKeycloakSessionFactory;

import io.quarkus.runtime.StartupEvent;
import lombok.extern.jbosslog.JBossLog;

@ApplicationScoped
@JBossLog
public class DemoConsumer {

    @Inject
    @Channel("words-out")
    Emitter<String> emitter;

    QuarkusKeycloakSessionFactory sessionFactory;

    @PostConstruct
    void init() {
    	sessionFactory = QuarkusKeycloakSessionFactory.getInstance();
    }
    
    /**
     * Sends message to the "words-out" channel, can be used from a JAX-RS resource or any bean of your application.
     * Messages are sent to the broker.
     **/
    void onStart(@Observes StartupEvent ev) {
        Stream.of("Hello", "with", "SmallRye", "reactive", "message").forEach(string -> emitter.send(string));
    }
    @Incoming("words-in")
    @Outgoing("uppercase")
    public Message<String> toUpperCase(Message<String> message) {
        return message.withPayload(message.getPayload().toUpperCase());
    }

    /**
     * Consume the uppercase channel (in-memory) and print the messages.
     **/
    @Incoming("uppercase")
    public void sink(String word) {
        System.out.println(">> " + word);
        //TODO: Quarkus stops processing Kafka if exception is thrown. I am doing try-catch now.
        try {
	    	KeycloakModelUtils.runJobInTransaction(sessionFactory, session -> {
	    		session.realms().getRealmsStream().forEach(r -> System.out.println(r.getName()));
	    	});
        } catch (Exception e) {
        	log.errorv("Error when processing sink", e);
        }
    }
    
    @Incoming("user-events")
    public void userCreated(UserCreatedEvent event) {
    	KeycloakModelUtils.runJobInTransaction(sessionFactory, session -> {
    		RealmModel realm = session.realms().getRealmByName("master");
    		session.users().addUser(realm, event.email);
    	});
		log.infov("User created {0}", event.email);
    }
}