package com.mc;

import io.quarkus.kafka.client.serialization.JsonbDeserializer;

public class UserCreatedEventDeserializer extends JsonbDeserializer<UserCreatedEvent>{

	public UserCreatedEventDeserializer() {
		super(UserCreatedEvent.class);
	}

}
