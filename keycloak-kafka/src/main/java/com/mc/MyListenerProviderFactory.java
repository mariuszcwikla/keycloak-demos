package com.mc;

import org.keycloak.Config.Scope;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;

import io.quarkus.arc.Arc;
import io.smallrye.reactive.messaging.kafka.KafkaClientService;

public class MyListenerProviderFactory implements EventListenerProviderFactory {

	private KafkaClientService clientService;

	@Override
	public EventListenerProvider create(KeycloakSession session) {
		return new MyEventListenerProvider(session, clientService);
	}

	@Override
	public void init(Scope config) {
	}

	@Override
	public void postInit(KeycloakSessionFactory factory) {
		clientService = Arc.container().instance(KafkaClientService.class).get();
	}

	@Override
	public void close() {

	}

	@Override
	public String getId() {
		return "mc-eventlistener";
	}

}
