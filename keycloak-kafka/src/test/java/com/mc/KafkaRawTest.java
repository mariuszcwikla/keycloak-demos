package com.mc;

import java.util.UUID;

import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.jupiter.api.Test;
import org.keycloak.quarkus.runtime.integration.QuarkusKeycloakSessionFactory;

import io.quarkus.kafka.client.serialization.JsonbSerializer;
import io.smallrye.reactive.messaging.kafka.companion.KafkaCompanion;
import lombok.extern.slf4j.Slf4j;

/**
 * WORK-IN-PROGRESS. DOES NOT WORK YET!
 * 
 * You need to run kafka and keycloak first.
 */
@Slf4j
class KafkaRawTest {

	@Test
	void test() {
		QuarkusKeycloakSessionFactory sessionFactory = QuarkusKeycloakSessionFactory.getInstance();
		
		KafkaCompanion companion = new KafkaCompanion("localhost:9092");
		companion.registerSerde(UserCreatedEvent.class, 
				new JsonbSerializer<UserCreatedEvent>(),
				new UserCreatedEventDeserializer()
			);
		String login = String.format("%s@foo.com", UUID.randomUUID().toString());
		companion.produce(UserCreatedEvent.class)
			.fromRecords(
				new ProducerRecord("user-events", new UserCreatedEvent(login))
			);
		
//		Awaitility.await()
//			.until(() -> {
//				
//			});
	}

}
