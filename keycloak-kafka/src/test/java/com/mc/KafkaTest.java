package com.mc;

import org.junit.jupiter.api.Test;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.quarkus.runtime.integration.QuarkusKeycloakSessionFactory;

import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.main.Launch;

/**
 * WORK-IN-PROGRESS. DOES NOT WORK YET!
 */
@QuarkusTest
class KafkaTest {

	@Test
	@Launch({ "start", "--http-enabled=true", "--hostname-strict=false"})
	void test1() {
		QuarkusKeycloakSessionFactory sessionFactory = QuarkusKeycloakSessionFactory.getInstance();
		KeycloakModelUtils.runJobInTransaction(sessionFactory, session -> {
			session.realms().getRealmsStream().forEach( r -> {
				//sys r.getName()
				System.out.println(r.getName());
			});
		});
	}

}
