package com.mc.keycloak.directgrant.sms;

import org.keycloak.representations.idm.OAuth2ErrorRepresentation;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SMSOAuth2ErrorRepresentation extends OAuth2ErrorRepresentation{
	private int resendsLeft;
}
