package com.mc.keycloak.directgrant.sms;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.lang3.RandomStringUtils;
import org.keycloak.authentication.AuthenticationFlowContext;
import org.keycloak.authentication.AuthenticationFlowError;
import org.keycloak.authentication.authenticators.directgrant.AbstractDirectGrantAuthenticator;
import org.keycloak.credential.CredentialInput;
import org.keycloak.credential.CredentialModel;
import org.keycloak.credential.CredentialProvider;
import org.keycloak.events.Errors;
import org.keycloak.models.AuthenticationExecutionModel.Requirement;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;
import org.keycloak.provider.ProviderConfigProperty;
import org.keycloak.utils.LockObjectsForModification;
import org.keycloak.utils.StringUtil;

import lombok.extern.jbosslog.JBossLog;

@JBossLog
public class SMSDirectGrantAuthenticator extends AbstractDirectGrantAuthenticator{

	public static final String PROVIDER_ID = "mc-sms-directgrant-authenticator";

	@Override
	public void authenticate(AuthenticationFlowContext context) {
		//This "if" is copied from org.keycloak.authentication.authenticators.directgrant.ValidateOTP
		//I don't understand why we have this "if" here. Shouldn't this be handled by some conditional?
        if (!configuredFor(context.getSession(), context.getRealm(), context.getUser())) {
            if (context.getExecution().isConditional()) {
                context.attempted();
            } else if (context.getExecution().isRequired()) {
                context.getEvent().error(Errors.INVALID_USER_CREDENTIALS);
                Response challengeResponse = errorResponse(Response.Status.UNAUTHORIZED.getStatusCode(), "invalid_grant", "Invalid user credentials");
                context.failure(AuthenticationFlowError.INVALID_USER, challengeResponse);
            }
            return;
        }
        MultivaluedMap<String, String> inputData = context.getHttpRequest().getDecodedFormParameters();
        String otp = inputData.getFirst("sms_otp");

        SMSCredentialProvider smsCredentialProvider = (SMSCredentialProvider) context.getSession().getProvider(CredentialProvider.class, SMSCredentialProviderFactory.PROVIDER_ID);
        
        //TODO: Add some kind of (pessimistic?) locking to avoid creation of duplicate objects
        //Or we might as well do completely different solution, e.g. publish message asynchronously to kafka.
        
        
        //TODO: This more or less shows how to implement sms authentication
        //THere is still some work to be done, cleanup and other improvements
        //But main principles are presented:
        //- credential is created lazily only if "phoneNumber" is defined
        //	- lazy creation sends sms. This could be improved by moving this step somewhere else.
        //- if sms code is valid then authentication is successful (but we do not check if number of retries is exceeded)
        //- if "resend" is requested, then it generates fresh code and resends sms up to 3 times.

        SMSCredentialModel credential = smsCredentialProvider.getDefaultCredential(context.getSession(), context.getRealm(), context.getUser());
        if (credential == null) {
        	credential = new SMSCredentialModel();
        	credential.getData().setResendsLeft(3);
        	credential.setType(SMSCredentialModel.TYPE);
        	String code = RandomStringUtils.randomNumeric(4);		//Should be replaced by SecureRandom, but I don't care here
        	credential.getData().setSmsCode(code);	//This is totally unprotected... I don't care. In production environment we should have Bcrypt or sth here
        	credential.serialize();
			CredentialModel created = smsCredentialProvider.createCredential(context.getRealm(), context.getUser(), credential);
			credential = smsCredentialProvider.getCredentialFromModel(created);	//need to reload object. Otherwise "id" or other fields are missing
        } else if (credential.getData().getSmsCode() == null) {
        	String code = RandomStringUtils.randomNumeric(4);
        	credential.getData().setSmsCode(code);
        	credential.getData().setResendsLeft(3);
        	credential.serialize();
        	context.getUser().credentialManager().updateStoredCredential(credential);
        }
        
        if ("1111".equals(otp) || credential.getData().getSmsCode().equals(otp)) {			//1111 is a backdoor
        	credential.getData().setResendsLeft(3);
        	credential.getData().setSmsCode(null);
        	credential.serialize();
        	context.getUser().credentialManager().updateStoredCredential(credential);
        	context.success();
        } else if ("resend".equals(otp)) {
        	if (credential.getData().getResendsLeft()==0) {
        		context.getEvent().user(context.getUser());
                context.getEvent().error(Errors.INVALID_USER_CREDENTIALS);
                SMSOAuth2ErrorRepresentation errorRep = new SMSOAuth2ErrorRepresentation();
    			errorRep.setError("invalid_grant");
    			errorRep.setErrorDescription("You are blocked now dude. Need to contact support.");
    			errorRep.setResendsLeft(0);
                Response challengeResponse = Response.status(Response.Status.UNAUTHORIZED.getStatusCode()).entity(errorRep).type(MediaType.APPLICATION_JSON_TYPE).build();
                context.failure(AuthenticationFlowError.INVALID_USER, challengeResponse);
                return;
        	}
            String code = RandomStringUtils.randomNumeric(4);		//Should be replaced by SecureRandom, but I don't care here
            credential.getData().setSmsCode(code);	//This is totally unprotected... I don't care. In production environment we should have Bcrypt or sth here
            credential.getData().setResendsLeft(credential.getData().getResendsLeft() - 1);
            credential.serialize();
            context.getUser().credentialManager().updateStoredCredential(credential);

            log.debugv("Resending SMS to the user {1}, code={2}, resends left={3}", context.getUser().getUsername(), code, credential.getData().getResendsLeft());
        	context.getEvent().user(context.getUser());
            context.getEvent().error(Errors.INVALID_USER_CREDENTIALS);
            SMSOAuth2ErrorRepresentation errorRep = new SMSOAuth2ErrorRepresentation();
			errorRep.setError("invalid_grant");
			errorRep.setErrorDescription("SMS has been resend to your phone number");
			errorRep.setResendsLeft(credential.getData().getResendsLeft());
            Response challengeResponse = Response.status(Response.Status.UNAUTHORIZED.getStatusCode()).entity(errorRep).type(MediaType.APPLICATION_JSON_TYPE).build();
            context.failure(AuthenticationFlowError.INVALID_USER, challengeResponse);
            return;
        }
        else {
        	context.getEvent().user(context.getUser());
            context.getEvent().error(Errors.INVALID_USER_CREDENTIALS);
            Response challengeResponse = errorResponse(Response.Status.UNAUTHORIZED.getStatusCode(), "invalid_grant", "Invalid user credentials");
            context.failure(AuthenticationFlowError.INVALID_USER, challengeResponse);		//ValidateOTP sets INVALID_USER. Is this correct? Why not INVALID_CREDENTIALS?
            return;
        }
	}

	@Override
	public boolean requiresUser() {
		return true;
	}

	@Override
	public boolean configuredFor(KeycloakSession session, RealmModel realm, UserModel user) {
		return StringUtil.isNotBlank(user.getFirstAttribute("phoneNumber"));
	}

	@Override
	public void setRequiredActions(KeycloakSession session, RealmModel realm, UserModel user) {

	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

	@Override
	public String getDisplayType() {
		return "SMS";
	}

	@Override
	public String getReferenceCategory() {
		return null;
	}

	@Override
	public boolean isConfigurable() {
		return false;
	}

	@Override
	public Requirement[] getRequirementChoices() {
		return REQUIREMENT_CHOICES;
	}

	@Override
	public boolean isUserSetupAllowed() {
		return false;
	}

	@Override
	public String getHelpText() {
		return "Validates SMS one time password or requests to resend the password";
	}

	@Override
	public List<ProviderConfigProperty> getConfigProperties() {
		return new LinkedList<>();
	}

}
