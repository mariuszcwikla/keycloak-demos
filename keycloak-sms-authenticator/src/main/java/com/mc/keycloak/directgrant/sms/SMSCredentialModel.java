package com.mc.keycloak.directgrant.sms;

import java.io.IOException;

import org.keycloak.credential.CredentialModel;
import org.keycloak.util.JsonSerialization;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class SMSCredentialModel extends CredentialModel{
	
	public static String TYPE = "mc-sms";

	private static final long serialVersionUID = 1L;

	@Data
	public static class SmsCredentialModelData{
		private Integer resendsLeft;
		private String smsCode;
	}
	
	private SmsCredentialModelData data = new SmsCredentialModelData();

	public void serialize() {
		try {
			String json = JsonSerialization.writeValueAsString(this.data);
			setCredentialData(json);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void deserialize() {
		try {
			this.data = JsonSerialization.readValue(getCredentialData(), SmsCredentialModelData.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
