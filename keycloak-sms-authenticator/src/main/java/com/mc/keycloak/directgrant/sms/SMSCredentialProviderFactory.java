package com.mc.keycloak.directgrant.sms;

import org.keycloak.credential.CredentialProviderFactory;
import org.keycloak.models.KeycloakSession;

public class SMSCredentialProviderFactory implements CredentialProviderFactory<SMSCredentialProvider> {

	public static final String PROVIDER_ID = "mc-sms-credential";

	@Override
	public SMSCredentialProvider create(KeycloakSession session) {
		return new SMSCredentialProvider(session);
	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

}
