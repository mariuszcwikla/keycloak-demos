package com.mc.keycloak.directgrant.sms;

import org.keycloak.common.util.Time;
import org.keycloak.credential.CredentialModel;
import org.keycloak.credential.CredentialProvider;
import org.keycloak.credential.CredentialTypeMetadata;
import org.keycloak.credential.CredentialTypeMetadataContext;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.UserModel;

public class SMSCredentialProvider implements CredentialProvider<SMSCredentialModel>{

	private KeycloakSession session;

	public SMSCredentialProvider(KeycloakSession session) {
		this.session = session;
	}
	@Override
	public String getType() {
		return SMSCredentialModel.TYPE;
	}

	@Override
	public CredentialModel createCredential(RealmModel realm, UserModel user, SMSCredentialModel credentialModel) {
        if (credentialModel.getCreatedDate() == null) {
            credentialModel.setCreatedDate(Time.currentTimeMillis());
        }
        if (credentialModel.getData().getResendsLeft() == null) {
        	credentialModel.getData().setResendsLeft(3);
        }
        return user.credentialManager().createStoredCredential(credentialModel);
	}

	@Override
	public boolean deleteCredential(RealmModel realm, UserModel user, String credentialId) {
		return user.credentialManager().removeStoredCredentialById(credentialId);
	}

	@Override
	public SMSCredentialModel getCredentialFromModel(CredentialModel model) {
		SMSCredentialModel smsModel = new SMSCredentialModel();
		smsModel.setUserLabel(model.getUserLabel());
		smsModel.setCreatedDate(model.getCreatedDate());
		smsModel.setType(SMSCredentialModel.TYPE);
		smsModel.setId(model.getId());
		smsModel.setSecretData(model.getSecretData());
		smsModel.setCredentialData(model.getCredentialData());
		smsModel.deserialize();
		return smsModel;
	}

	@Override
	public CredentialTypeMetadata getCredentialTypeMetadata(CredentialTypeMetadataContext metadataContext) {
        return CredentialTypeMetadata.builder()
                .type(getType())
                .category(CredentialTypeMetadata.Category.TWO_FACTOR)
                .displayName("sms-display-name")
                .helpText("sms-help-text")
                .iconCssClass("kcAuthenticatorSMSClass")
                .createAction(null)
                .removeable(false)
                .build(session);
	}

}
