package com.mc;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

import org.keycloak.OAuthErrorException;
import org.keycloak.adapters.ServerRequest.HttpFailure;
import org.keycloak.adapters.installed.KeycloakInstalled;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;

public class LoginDesktopDemo {

	public static void main(String[] args) throws IOException, VerificationException, OAuthErrorException, URISyntaxException, HttpFailure, InterruptedException {
		KeycloakInstalled keycloak = new KeycloakInstalled();

		keycloak.loginDesktop();

		AccessToken token = keycloak.getToken();

		long minValidity = 30L;
		String tokenString = keycloak.getTokenString(minValidity, TimeUnit.SECONDS);

		System.out.println(tokenString);
		keycloak.logout();		//Here app waits until you click "Logout" in browser
								//This step might as well be removed.
	}

}
