package com.mc;

import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.keycloak.OAuthErrorException;
import org.keycloak.adapters.ServerRequest.HttpFailure;
import org.keycloak.adapters.installed.KeycloakInstalled;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;

/**
 * In Keycloak:
 * - add 
 * @author mariu
 *
 */
public class LoginCLIDemo {

	public static void main(String[] args) throws IOException, VerificationException, OAuthErrorException, URISyntaxException, HttpFailure, InterruptedException {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("META-INF/keycloak-cli.json");
		KeycloakInstalled keycloak = new KeycloakInstalled(is);
		keycloak.setLocale(new Locale("fr"));
		keycloak.loginCommandLine();

		AccessToken token = keycloak.getToken();

		long minValidity = 30L;
		String tokenString = keycloak.getTokenString(minValidity, TimeUnit.SECONDS);

		System.out.println(tokenString);
		keycloak.logout();		//Here app waits until you click "Logout" in browser
								//This step might as well be removed.
	}

}
